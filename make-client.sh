#!/bin/bash
# TODO: Convert this to a damn makefile
CC=clang
CFLAGS="-O2 -Wall"
BUILD_DIR=./build
CLI_DIR=./client
SRV_DIR=./server
COM_DIR=./common

${CC} -c ${CFLAGS} client_main.c -o ${BUILD_DIR}/client_main.o 
${CC} -c ${CFLAGS} ${CLI_DIR}/client.c -o ${BUILD_DIR}/client.o 
${CC} -c ${CFLAGS} ${CLI_DIR}/cli-packet.c -o ${BUILD_DIR}/cli-packet.o 

${CC} ${BUILD_DIR}/{client_main.o,client.o,cli-packet.o} -o cli_main ${CFLAGS}
