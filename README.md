# Project-com
## An asynchronous communication protocol with a persistant log for all users.

###SERVER:
- Listens for connections
- Holds a database of all registered users
      * Each user has a dedicated "key"
      * This is for verifying who said what, and keeping each user's messages encrypted
- Holds a database for all (signed) messages
      * Each message will be encrypted on send, and decrypted on receive
      * Each message will be timestamped (according to server)
      * Each message "pull" will send the server the "last" timestamp the local client has
      * Server sends back the next sequential timestamp and message
- Verifies each user
      * Can revoke certain keys access
      * Messages can only be decrypted with the intended recipient's key.
        
###CLIENT:
- Registers a user
      * Generates a key to encrypt messages for sending and receiving
      * All traffic to and from the user will be encrypted with that key
      * Symmetric key system
- Send a message
      * Marks it as a new message from the user, and encrypts with the user's key
      * Receives an acknowledgement that the message was received.
- Pull a message
      * Sends a "check for new messages" packet containing timestamp of the last message
      * Receives a number of new messages, following with any new messages (old->new)
      * Decrypts the message, then stores it in the local database
        

###POSSIBLE PACKET STRUCTURE:
> [data length][userid][packet type][data][CRC]
>
- packet type:
    * A one-byte unique ID for the packet declaring what it is
       + Log-in request, acknowledgement, update messages, etc.
- userid:
    * Four-byte(?) unique number to identify the user sending the packet
    * *NOT* the same as the user's encryption key.
- data length:
    * Four-byte(?) unsigned number that declares how long the data section is
- data:
    * ENCRYPTED data that is the of the length defined in the previous section
    * Is treated as binary form when decrypted, therefore is datatype agnostic.
- CRC:
    * Four-byte value to verify the encrypted data has been received correctly.
    * Calculated on the ENCRYPTED data just before sending, and right before decryption.
    
###PACKET TYPES:
- [Ack]: Confirms the other side received an error-free packet, may contain timestamp
- [Msg]: Contains encrypted message for submission into the server database
- [Rsd]: Resend last sent packet
- [UpR]: Request new messages, data field contains last message's timestamp
- [UpX]: Data field contains number of new messages (can be zero)
- [Snd]: Submit new message, data field contains the encrypted user's message
- [LgI]: Log in request with a given userid/password encrypted with a calculated key
- [LgG]: Log in granted
- [LgR]: Log in rejected
- [DsC]: Disconnect by user
- [Kal]: Keep-alive packet. If no UpR in awhile, send this. If no ACK, then disconnect.
- [Rev]: userid revocation, signal to delete local database and force disconnect.

###INTENDED CLIENT/SERVER INTERACTION:  

              SERVER: Initializes database of messages, and users  
CLIENT -LgI-> SERVER: Requests log in with given username and password  
              SERVER: Checks login validity, store connected users' IPs in a linked list  
SERVER -LgG-> CLIENT: Log on request granted  
              CLIENT: Load local database of messages  
CLIENT -UpR-> SERVER: Requests new messages, last message received was at YYYY  
              SERVER: Find timestamp in database, or the closest one (if error)  
SERVER -UpX-> CLIENT: There are X new messages since YYYY  
              CLIENT: Prepare to receive X new messages  
CLIENT -Ack-> SERVER: Ready to receive X messages  
SERVER -Msg-> CLIENT: Message at YYYZ  
              CLIENT: Check CRC, decrypt and store in database  
CLIENT -Ack-> SERVER: Send acknowledgement packet  
SERVER -Msg-> CLIENT: Message at YYZZ (etc.)  
              CLIENT: Check CRC, CRC failed  
CLIENT -Rsd-> SERVER: Resend the last message  
SERVER -Msg-> CLIENT: Sends the previous message again  
              CLIENT: CRC succeeds, decrypt message and store in the database  
CLIENT -Snd-> SERVER: Submit new message at time ZZZZ  
              SERVER: Verify CRC, and decrypt using sender's key  
SERVER -Ack-> CLIENT: Acknowledgement packet sent with the timestamp of the message  
              CLIENT: Adds own message to personal database with the received timestamp  
              SERVER: Store message in database, timestamp at the time of receive  
              CLIENT: User disconnects  
CLIENT -Dsc-> SERVER: User-enacted disconnect packet sent  
              SERVER: Close socket(s), remove user from linked list  
           
###POTENTIAL PROBLEMS:
- Data may not actually be characters or any useable format
- Databases hold all the "secure" data
- userids are not encrypted, vulnerable to tracing messages
- Like anything, vulernable to DDoS attacks
- Revoke packets might be spoofable if target user's key is obtained
    
    
###SUMMARY:
- All messages are encrypted and stored server-side (with an admin-set timeout)
- All data sent to the server is encrypted by the user's key
- All data sent to the user is encrypted by the user's key
- A user without a proper userid+key pair can not properly decrypt the messages
- Server will reject packets sent from an unknown, or "revoked" userid
- User will pull all new messages at a user-defined interval
- Messages are stored locally and remotely
- Revoke packet type set to clear revoked user's database (encrypted with user's key)
