CC=gcc
CFLAGS=-O2 -Wall
BUILD_DIR=./build
CLI_DIR=./client
SRV_DIR=./server
COM_DIR=./common

srv-main: ${BUILD_DIR}/srv-main.o server.o,srv-config.o,srv-packet.o
	gcc ${BUILD_DIR}/{srv-main.o,server.o,srv-config.o,srv-packet.o} -o srv_main

${BUILD_DIR}/srv-main.o: server_main.c ${SRV_DIR}/{server.*,srv-config.*,srv-error.h,srv-packet.*}
	gcc -c server_main.c -o ${BUILD_DIR}/server_main.o

${BUILD_DIR}/server.o: ${SRV_DIR}/{server.c,server.h,srv-config.*,srv-error.h,srv-packet.*}
	gcc -c ${SRV_DIR}/server.c -o ${BUILD_DIR}/server.o

${BUILD_DIR}/srv-config.o: ${SRV_DIR}/{srv-config.*,srv-error.h}
	gcc -c ${SRV_DIR}/srv-config.c -o ${BUILD_DIR}/srv-config.o

${BUILD_DIR}/srv-packet.o: ${SRV_DIR}/{srv-packet.h,srv-packer.c,server.h}
	gcc -c ${SRV_DIR}/srv-packet.c -o ${BUILD_DIR}/srv-packet.o
