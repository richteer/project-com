#ifndef _server_h_
#define _server_h_

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <wait.h>

#define EMPTY_DATA ~0x0
#define USER_SECTION unsigned short
#define TYPE_SECTION unsigned char
#define DATA_SECTION unsigned short
#define CRC_SECTION unsigned char

#define TRUE 1
#define FALSE 0
#define BOOL unsigned short
#define VERBOSE 1

#include "srv-config.h"
#include "srv-error.h" // TODO: Fix this library
#include "srv-packet.h"
#include "conlist.h"

//extern enum { FALSE, TRUE } BOOL;

extern unsigned short SERVER_READY;

//extern BOOL SERVER_READY = FALSE;

// Get the server ready to go, returns 0 if there were no problems detected.
//  Sets the input to the socket that is bound
//  Also sets the boolean SERVER_READY to true if completed successfully.
int server_init(int* sockfd, struct config* conf);

// Main server loop, call this after server_init(), bails if SERVER_READY is false
//  Returns 0 if the server was shut down by user, and an error code if else
int server_main(int* sockfd, struct config* conf);

// Signal Handlers
//TODO: Learn how this works
// NOTICE: The below code has been graciously borrowed from Beej's Guide to Network Programming.
//   I am borrowing to avoid a zombie-process problem in the future, this is to be replaced later


#endif
