#include "srv-config.h"

#define PORT "12345"
#define MAX_PORT "12349"

int get_config(struct config* conf)
{
    if (conf == NULL)
    {
        conf = (struct config*) calloc(1,sizeof(struct config));
        if (!conf)
        {
            return (MEM_ALLOC_FAIL);
            //return (srverrno = MEM_ALLOC_FAIL);
        }
    }
    
    fprintf(stdout,"Loading configuration...\n");
    
    // TODO: Get configuration logic goes here
    
    strcpy(conf->port,PORT);
    strcpy(conf->max_port,MAX_PORT);
    
    if (VERBOSE)
    {
        fprintf(stdout,"Configuration successfully loaded.\n");
    }
    return SUCCESS;
}

