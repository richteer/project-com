#include "server.h"


// NOTE: This exists only because fork() is being used
static void sigchld_handler(int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}


// TODO: Have this function handle config file settings, using #defs for now
int server_init(int* sockfd, struct config* conf)
{
    if (VERBOSE)
    {
        fprintf(stdout,"Initializing Server\n");
    }

    // TODO: Use this variable to catch and report errors
    //int err;
    
    // Start preparing the socket
    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    // For IPv4 or IPv6... TODO: Be IPv6 compatible
    hints.ai_socktype = SOCK_DGRAM; // Use a UDP connection to be asynchronous
    hints.ai_flags = AI_PASSIVE;
    
    getaddrinfo(NULL, conf->port, &hints, &res);
    *sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    
    if (VERBOSE)
    {
        fprintf(stdout,"Datagram Socket created\n");
    }
    
    // TODO: Implement port range switching (in case the first fails)
    if (bind(*sockfd, res->ai_addr, res->ai_addrlen) == -1)
    {
        //srverrno = BIND_FAIL;
        //srverror("Server Init");
        perror("Server Init");
        return BIND_FAIL;
    }
    
    if (VERBOSE)
    {
        fprintf(stdout,"Socket bound successfully\n");
    }
    
    if (VERBOSE)
    {
        fprintf(stdout,"Server Initialized Successfully!\n");
    }


    freeaddrinfo(res);
    
    SERVER_READY = TRUE;
    return SUCCESS;
}

int server_main(int* sockfd, struct config* conf)
{
    if (!SERVER_READY)
    {
        fprintf(stderr,"Server is not ready\n");
        return 1;
    }

    /*if (listen(*sockfd,10) == -1)
    {
        srverrno = LISTEN_FAIL;
        srverror("Server Main Listen");
        perror("Server Main Listen");
        return LISTEN_FAIL;
    }*/

    BOOL quit = FALSE;
    
    DATA_SECTION data_len;          // Header of the packet, length of the data section
    
    
    /* Note: this is deprecated. (already) Switched to a struct to make the switch 
            to pthreads easier.
    USER_SECTION userid;            // User ID number of the sender
    TYPE_SECTION type;              // Type of packet, for redirection to the proper function
    CRC_SECTION data_crc;           // CRC the client calculated on the user+data section
    */
    
    // More child process-killing borrowed code... TODO: Write my own, dammit.
    
    struct sigaction sa;
    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    {
        perror("sigaction");
        exit(1);
    }
    // ^-- End borrowed code
    
    struct sockaddr_storage from;
    unsigned fromlen = sizeof(from);
    int bufferlen, bytes;
    void* buffer;
    
    //TODO: SWITCH TO PTHREADS
    int pid;
    struct packet pk;
    // Master loop for accepting packets, redirects to the packet handling function(s)
    fprintf(stdout,"Server started successfully, listening on UDP port %s\n",conf->port);
    // Note, update above when the fallback port switching works. Who cares right now
    
    // Create the pointer to a new connection list.
    //  NOTE: This is a constant pointer that should NOT be changed.
    //  However, its contents may be changed, this is so all the forks/threads don't create copies of the same
    //  structure.
    struct list* conlist = (struct list*) calloc(1,sizeof(struct list*));
    while (!quit)
    {
        // Get the first two bytes of data that holds the size of the data section
        //  Also, only "peeks" at the data, so the next recvfrom can get all the data in one pull
        //   This prevents over-, or under-, buffering, since we know how much data to expect.
        // TODO: Check bytes received
        recvfrom(*sockfd, &data_len, sizeof(data_len), MSG_PEEK,(struct sockaddr*) &from, &fromlen);
        
        // If the data section is empty, signified by 0xF..F
        if (data_len == (unsigned short)(EMPTY_DATA)) // FIXME: get rid of the cast
        ;
        
        // OPTIMIZE: Get rid of the sizeof() calls
        bufferlen = sizeof(DATA_SECTION) + sizeof(USER_SECTION) + sizeof(TYPE_SECTION) + data_len + sizeof(CRC_SECTION);
        buffer = calloc(1,bufferlen);
        
        if (VERBOSE)
        {
            fprintf(stdout,"Packet incoming!\n");
        }
        
        // Pull the rest of the packet into a buffer
        // TODO: Ensure the proper amount of bytes are read in
        bytes = recvfrom(*sockfd, buffer, bufferlen, 0,(struct sockaddr*) &from, &fromlen);
        
        if (VERBOSE)
        {
            fprintf(stdout,"Obtaining packet data...\n");
        }
        
        if (bytes != bufferlen)
        {
            fprintf(stderr, "Did not read in the expected number of bytes\n");
        }

        // Create a struct with the packet information
        //   NOTE: This is to create a copy of the data, so *buffer doesn't get overwritten

        pk = build_packet(buffer,bufferlen);
        free(buffer);   // Buffer is no longer needed, free it up for the next incoming packet
        
        // OPTIMIZE: Move this stuff back into the packet handler for security reasons.
        if (pk.type == LOGIN)
        {
            if (login_request(&pk, conlist,sockfd))
            {
                // Login failed logic - send failure packet
            }
            else
            {
                // Login succeed logic - send success packet
            }
            goto NEXT; // TODO: Clean this up, using a GOTO for now to avoid an uneeded fork
        }
        else if (pk.type == DISCONNECT)
        {
            // execute disconnect code here
        }
        
        
        // TODO: IN CASE YOU MISSED THE LAST ONE, SWITCH TO PTHREADS
        // Let the data packet be processed in another process (hehe)
        //   This way the server can keep taking in new packets
        // TODO: Error-check the fork if it's staying. Pick one, dammit.

        pid = fork();
        if (pid == 0)
        {
            handle_packet(pk);
            exit(0);
        }
        NEXT:
        // To silence the error about expected statement. (This way the while condition is still checked)
        // TODO: Remove the need for this.
        printf("");
    }
    
    
    // To silence a warning, currently will be an infinite loop.
    //   Suggestion: use another thread and send a signal to end
    return 0;
}
