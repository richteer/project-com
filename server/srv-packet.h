#ifndef _srv_packet_h_
#define _srv_packet_h_
//#include "server.h"

// All the packet types, for the redirector
#define LOGIN 0x1           // Log in request
#define LOGIN_GRANTED 0x2   // Allow the client to start sending messages
#define LOGIN_REJECTED 0x3  // User/Pass mismatch, or revoked user. Reason in [data] section (string)
#define DISCONNECT 0x4      // Disconnect by user
#define ACK 0x5             // Acknowledgement packet
#define MSG 0x6             // Message Packet
#define RESEND 0x7          // Resend last sent packet (failed CRC, etc.)
#define UPDATE_REQ 0x8      // Request for new updates
#define UPDATE_NUM 0x9      // Notification of how many new messages to expect
#define SEND 0xA            // Submission of a new message
#define REVOKE              // User Killswitch, signal to client to remove database, and force disconnect

// #defines from server.h, TODO: Figure out a nicer way to handle this
#define EMPTY_DATA ~0x0
#define USER_SECTION unsigned short
#define TYPE_SECTION unsigned char
#define DATA_SECTION unsigned short
#define CRC_SECTION unsigned char

#define TRUE 1
#define FALSE 0
#define BOOL unsigned short
#define VERBOSE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "srv-error.h"
#include "conlist.h"

// OPTIMIZE: Reorder this when the data types are settled
struct packet
{
    USER_SECTION userid;
    TYPE_SECTION type;
    void* data;
    DATA_SECTION datalen;
    CRC_SECTION crc;
};

// Build a struct for the packet handler to handle.
struct packet build_packet(void* buffer, int length);

// Redirect the packet to the proper packet type handler
int handle_packet(struct packet pk);

// Calculate and check the CRC value of the obtained packet against the supposed CRC
//  TODO: Update this with a more advanced CRC
// OPTIMIZE: Use the BOOL enum or macro instead
unsigned char crc_check(struct packet* pk);

// TODO: Check CRC prior to redirecting packet, return something if packet failed
// TODO: Write this: it's the pthread version
// Function to redirect the packet to the proper packet type's handler
//void* handle_packet(void* args);



/*\**************************\*/
/*\***** Packet Handlers ****\*/
/*\**************************\*/

/*** LOGIN Related functions ***/
// Checks the data field for a valid encrypted username/pass pair
int login_request(struct packet* pk, struct list* L, int* sockfd);

#endif

