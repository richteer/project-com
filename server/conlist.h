// A small linked list library for storing the connection information as connections are established

#ifndef _conlist_h_
#define _conlist_h_
#include <stdlib.h>

#define USER_SECTION unsigned short
#define DEBUG 1
// Put the information the server needs to know about each CONNECTION in here
struct node
{
    USER_SECTION userid;
    // TODO: IP Address goes here, etc.
    struct node* next;
};

// Struct holding the start and end of the list, each fork will have a *pointer* to this, rather than a copy
//   Also simplifies the functions to modify the list, since there only needs to be one argument
// IMPORTANT NOTE: If this is going to be free()'d for WHATEVER REASON, run free_list() on it FIRST.
//   Otherwise, the list itself will not be removed, just all connections we have to it instead (Memory leak)
struct list
{
    struct node* head;
    struct node* tail;
};

// Append a connection to the list, start the list if empty
//  Returns a pointer to the new node
struct node* add_node(struct list* L, USER_SECTION uid /*Add other information here*/);

// Remove a node from a pointer to a specific node.
//  NOTE: This probably shouldn't be called from the program, and rather just internally from rem_node_uid()
//   Returns zero if the node was removed successfully, anything else means something messed up
int rem_node_ptr(struct list* L, struct node* target);

// Find and return a pointer to the node with the supplied uid. Returns a NULL pointer if not found
struct node* find_node_uid(struct list* L, USER_SECTION uid);

// Free up the entire list. This is probably not needed, but included in case of crashes, manual server resets,
//  or whatever we can think of that might need this.
int free_list(struct list* L);

#ifdef DEBUG
// A debug list that is not really needed for the main release
#include <stdio.h>
void dump_list(struct list* L);
#endif

#endif
