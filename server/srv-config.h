#ifndef _srv_config_h_
#define _srv_config_h_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
// TODO: Fix the srv-error library
#include "srv-error.h"
#define VERBOSE 1
//unsigned short srverrno;

// Holds all the configuration information for server_init()
struct config 
{
    char port[6];       // First port to attempt to bind on
    char max_port[6];   // (optional) maximum port to bind (in case of the initial is in use)
    // TODO: char* motd;     // Message of the day to send to clients
};

// Gathers configuration from the config file.
//  NOTE: Pass in an empty pointer to allocate new space, otherwise uses as-is
int get_config(struct config* conf);

#endif
