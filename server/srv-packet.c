#include "srv-packet.h"

struct packet build_packet(void* buffer, int length)
{
    struct packet pk;
    
    // Pre-calculate to avoid unecessary repeated method calls
    //  OPTIMIZE: Remove this with hard coded "good" values
    // CLEANUP: Reorganize these, and replace with cleaner variable names
    unsigned size_datalen = sizeof(DATA_SECTION); // Size of the section holding the datasize
    unsigned size_user = sizeof(USER_SECTION);
    unsigned size_type = sizeof(TYPE_SECTION);
    unsigned size_crc = sizeof(CRC_SECTION);
    unsigned size_data = length - (size_user + size_type + size_crc + size_datalen);
    
    if (VERBOSE) {
        fprintf(stderr,"Packet pointers:\n \
        datalen: %p, offset %u\n \
        userid: %p, offset %u \n \
        type: %p, offset %u \n \
        crc: %p, offset %u \n \
        data: %p, offset %u \n",
        &buffer[0],0,
        &buffer[size_datalen],size_datalen,
        &buffer[size_datalen+size_user],size_datalen+size_user,
        &buffer[size_datalen+size_user+size_type+size_data],size_datalen+size_user+size_type+size_data,
        &buffer[size_datalen + size_user + size_type],size_datalen + size_user + size_type);
    }
    
    // FIXME: Make this less of a disgusting to read section, works, but barely
    pk.datalen = *((DATA_SECTION*) &buffer[0]);
    pk.userid = *((USER_SECTION*) &buffer[size_datalen]);
    pk.type = *((TYPE_SECTION*) &buffer[size_datalen+size_user]);
    pk.crc = *((CRC_SECTION*) &buffer[size_datalen+size_user+size_type+size_data]);
    pk.data = calloc(size_data,1);
    memcpy(pk.data,&buffer[size_datalen + size_user + size_type],size_data);
    
    return pk;
}

// This should be run in a forked process, redirects the packet to the proper interpreter
int handle_packet(struct packet pk)
{
    if (VERBOSE)
    {
        fprintf(stdout,"Packet information:\n\
        Data length: %hu\n\
        User ID: %X\n\
        Type: %X\n\
        CRC: %X\n\
        Data: %s\n",
        pk.datalen,pk.userid,pk.type,pk.crc,pk.data);
    }
    if (!crc_check(&pk))
    {
        fprintf(stderr,"CRC Check failed!\n");
        return CRC_FAIL;
    }
    // Beware the macros, all will be defined in srv-packet.h
    switch(pk.type)
    {
        case LOGIN:
            // Run the login process thingy
            // return error code if there's a problem
            printf("Userid %hu has requested a LOGIN\n",pk.userid);
            break;
    }
    
    // TODO: Maybe put this in the packet handlers?
    // Packet Data is no longer needed, free it up to prevent memory leaking
    free(pk.data);
    return SUCCESS;
}

unsigned char crc_check(struct packet* pk)
{
    unsigned char calc_crc = 0xFF;
    int i;
    for (i = 0; i < pk->datalen; i++)
        calc_crc ^= ((char*) pk->data)[i];
    
    if (VERBOSE)
    {
        fprintf(stdout,"Calculated CRC is %X, Stored CRC is %X\n",calc_crc,pk->crc);
    }
    
    return (calc_crc == pk->crc);
}

/*\**************************\*/
/*\***** Packet Handlers ****\*/
/*\**************************\*/

/*** LOGIN Related functions ***/

int login_request(struct packet* pk, struct list* L, int* sockfd)
{
    // Check first that the user has not already connected, or is revoked
    if (find_node_uid(L,pk->userid) != NULL)
    {
        // The value could not be found, so the idiot are logged in, or we are being spoofed...
        // TODO: Check IP Address here, if match, then the user got disconnected, or is an idiot.
        fprintf(stdout,"Received a login request from already connected user %u\n",pk->userid);
        return 2;
    }
    
    
    
    // TODO: Put ACTUAL verification logic here. For testing, each pass is the userid
    unsigned short data = *((unsigned short*) pk->data);
    if (memcmp(&data,&pk->userid,sizeof(USER_SECTION)))
    {
        fprintf(stdout,"User %u failed to log in\n",pk->userid);
        return 1;
    }
    
    add_node(L,pk->userid);
    fprintf(stdout,"User %u logged in successfully\n",pk->userid);
    fprintf(stdout,"Connected users:\n");
    dump_list(L);
    return 0;
}
