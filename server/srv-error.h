// Holds all the wonderful server-side error codes and a code-to-stderr converter.

// IMPROVE: Find a better way to handle errors safely

#ifndef _srv_error_h_
#define _srv_error_h_

#define SUCCESS 0x0
#define MEM_ALLOC_FAIL 0x1
#define BIND_FAIL 0x2
#define LISTEN_FAIL 0x3
#define CRC_FAIL 0x4



//extern unsigned short srverrno;

/*
extern void srverror(char* hint)
{
    fprintf(stderr,"%s: %u\n",hint,srverrno);
}*/

#endif
