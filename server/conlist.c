#include "conlist.h"

struct node* add_node(struct list* L, USER_SECTION uid /*Add other information here*/)
{
    // Note: this should /never/ have to be run, this is in case the linked list gets screwed up
    if ((L->head == NULL) ^ (L->tail == NULL))
        return NULL;

    // If head & tail are null, then the list is empty
    if ((L->head == NULL) && (L->tail == NULL))
    {
        L->tail = (L->head = (struct node*) malloc(sizeof(struct node)));
        L->head->next = NULL;   // Ensure that the first node's next value is null
        L->head->userid = uid;
        return L->head;
    }
    
    // If it gets here, then both head and tail are defined, meaning its a non-empty list
    
    // Create a new node, and have the tail point to it
    L->tail->next = (struct node*) malloc(sizeof(struct node));
    // Update tail to point to the new last node
    L->tail = L->tail->next;
    // Set the new last node's next to NULL
    L->tail->next = NULL;
    // Throw in all the data into the node
    L->tail->userid = uid;
    
    return L->tail;
}


int rem_node_ptr(struct list* L, struct node* target)
{
    // Note: this should /never/ have to be run, this is in case the linked list gets screwed up
    if ((L->head == NULL) ^ (L->tail == NULL))
        return -1;
    
    // In case the linked list is reported as empty
    if ((L->head == NULL) && (L->tail == NULL))
        return 1;
    
    // In case the genius who called this dumped in a null pointer...
    if (target == NULL)
        return 2;

    // If the target removed node is the ONLY node in the list
    if ((target == L->head) && (target == L->tail))
    {
        free(target);
        L->head = L->tail = NULL;
        return 0;
    }

    // Handle if the target node is the first one, and the list is at least two items long
    if (target == L->head)
    {
        // Have head now point to the second item
        L->head = target->next;
        // Clear up the target
        free(target);
        return 0;
    }
    
    // If target is the last item, unfortunately this means we have to recurse through the whole list to 
    //   find the node previous to the tail
    if (target == L->tail)
    {
        struct node* cur = L->head;
        int c = 0;
        while (cur != NULL)
        {
            if (cur->next == target)
            {
                c = 1;
                break;
            }
            cur = cur->next;
        }
        
        // If this triggers, there's something seriously wrong with the list...
        if (!c)
            return -1;
        
        // Set the new last node's next to NULL
        cur->next = NULL;
        // Set the tail to the new last node
        L->tail = cur;
        // Free up the target node
        free(target);
        return 0;
    }

    // If the code gets here, then it is assumed that the the list is greater than two items, and target is
    //  Neither the first nor the last
    struct node* cur = L->head;
    int c = 0;
    while (cur != NULL)
    {
        if (cur->next == target)
        {
            // The target is found in the list, proceed to remove, cur holds "previous"
            c = 1;
            break;
        }
        cur = cur->next;
    }
    
    // If the pointer was not found in the list, then bail out
    if (!c)
        return 3;
    
    // Have the previous node now point the the node after the target
    cur->next = target->next;
    // Free up the target node
    free(target);
    return 0;
}

struct node* find_node_uid(struct list* L, USER_SECTION uid)
{
    // Note: this should /never/ have to be run, this is in case the linked list gets screwed up
    if ((L->head == NULL) ^ (L->tail == NULL))
        return NULL;
    
    // If the list is empty, then the target uid can't be found
    if ((L->head == NULL) && (L->tail == NULL))
        return NULL;
    
    // The following two IFs check the first and last node for the matching userid. This is a potential speed
    //   increase if the target uid is the first or last node.
    if (L->head->userid == uid)
        return L->head;
    
    if (L->tail->userid == uid)
        return L->tail;
        
    struct node* cur = L->head;
    while (cur != NULL)
    {
        if (cur->userid == uid)
            return cur;
        cur = cur->next;
    }
    
    // If it gets here, then the userid was not found in the list
    return NULL;
}


// NOTE: Since this has no current use in the long run of the server, it is the laziest written.
int free_list(struct list* L)
{
    struct node* cur = L->head;
    struct node* temp;
    while (cur != NULL)
    {
        temp = cur->next;
        free(cur);
        cur = temp;
    }
    // For good measure, set the head and tail pointers to NULL, even though that struct is probably going to
    //   be removed too if the whole list is being cleared...
    L->head = NULL;
    L->tail = NULL;
    return 0;
}

#ifdef DEBUG

void dump_list(struct list* L)
{
    int c = 0;
    struct node* cur = L->head;
    while (cur != NULL)
    {
        printf("%i: %u\n",c++,(cur == NULL) ? 0 : cur->userid);
        cur = cur->next;
    }
    return;
}

#endif
