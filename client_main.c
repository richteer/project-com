// NOTE: At the moment this is a very stripped down version of the client. It is more of a "custom 
//   packet sender" than a client. This is mostly written to test the server, so excuse the even messier code

#include "client/client.h"

int main(int argc, char** argv)
{
    
    client_main();
    
    return 0;
}
