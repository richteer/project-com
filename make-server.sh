#!/bin/bash
# TODO: Convert this to a damn makefile
CC=clang
CFLAGS="-O2 -Wall"
BUILD_DIR=./build
CLI_DIR=./client
SRV_DIR=./server
COM_DIR=./common

${CC} -c ${CFLAGS} server_main.c -o ${BUILD_DIR}/server_main.o 
${CC} -c ${CFLAGS} ${SRV_DIR}/server.c -o ${BUILD_DIR}/server.o 
${CC} -c ${CFLAGS} ${SRV_DIR}/srv-config.c -o ${BUILD_DIR}/srv-config.o 
${CC} -c ${CFLAGS} ${SRV_DIR}/srv-packet.c -o ${BUILD_DIR}/srv-packet.o 
${CC} -c ${CFLAGS} ${SRV_DIR}/conlist.c -o ${BUILD_DIR}/conlist.o 

${CC} ${BUILD_DIR}/{server_main.o,server.o,srv-config.o,srv-packet.o,conlist.o} -o srv_main ${CFLAGS}
