/* Entry point for the server, initializes and checks for errors prior to entering
 *   the main server loop. If anything goes wrong, reports errors and exits immediately.
 */
#include <signal.h>
#include "server/server.h"
#include "server/srv-config.h"
#define VERBOSE 1
unsigned short SERVER_READY;
unsigned short srverrno;

// To handle the interrupt signal, note that this will probably be overwritten by deeper levels to safely die
void sigint(int sig)
{
    fprintf(stderr,"Interrupt signal received! Quitting...\n");
    exit(0);
}

int main(int argc, char** argv)
{
    int err = 0;
    SERVER_READY = FALSE;
    // Struct to hold all the server's configurations in
    struct config conf;
    
    signal(SIGINT,sigint);
    
    // Master socket to listen on, this will be set by server_init();
    int list_socket;
    
    // Obtain configuration, bail if there is an error
    if ((err = get_config(&conf)))
        return srverrno;
        
    // Get the socket ready to go, give up if there is something wrong
    if ((err = server_init(&list_socket,&conf)))
        return srverrno;
        
    // Start the server, and when it closes, return the reason for close
    //   NOTE: 0 = clean shutdown
    if ((err = server_main(&list_socket,&conf)))
        return srverrno;
    
    return 0;
}
