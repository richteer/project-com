#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#define USER_SECTION unsigned short
#define TYPE_SECTION unsigned char
#define DATA_SECTION unsigned short
#define CRC_SECTION unsigned char


// NOTE: Most of this program was directly copied from source files for other components
//   This is only a tester program

struct packet
{
    USER_SECTION userid;
    TYPE_SECTION type;
    void* data;
    DATA_SECTION datalen;
    CRC_SECTION crc;
};

void* uctToptr(struct packet pk, int* length)
{
    size_t size_user = sizeof(USER_SECTION);
    size_t size_type = sizeof(TYPE_SECTION);
    size_t size_crc = sizeof(CRC_SECTION);
    size_t size_data = pk.datalen;
    *length = size_user+size_type+size_crc+size_data+sizeof(DATA_SECTION);
    void* message = malloc(*length);
    memcpy(&(message[0]),&pk.datalen,sizeof(DATA_SECTION)); // Put the data section length in first
    memcpy(&(message[sizeof(DATA_SECTION)]),&pk.userid,size_user); // Then the senderid
    memcpy(&(message[sizeof(DATA_SECTION)+size_user]),&pk.type,size_type);  // Then the packet type
    // Skip the message itself for now
    
    memcpy(&(message[sizeof(DATA_SECTION)+size_user+size_type]),pk.data,pk.datalen);
    
    memcpy(&(message[size_user+size_type+size_data+sizeof(DATA_SECTION)]),&pk.crc,size_crc); //CRC is last
    
    return message;
}


int main(int argc, char** argv)
{
    if (argc != 4)
    {
        printf("Usage: client <target ip or hostname> <port> <packet input file>\n");
        printf("\n  Example: client 127.0.0.1 12345\n");
        return 1;
    }
    struct packet pk;
    
    FILE * inpack = fopen(argv[3],"r");
    
    if (!inpack)
    {
        fprintf(stderr,"Could not read from file!\n");
        return 1;
    }
    char* tempbuf = (char*) calloc(1,256);
    fgets(tempbuf,256,inpack);
    pk.userid = (unsigned short) atoi(tempbuf);
    fgets(tempbuf,256,inpack);
    pk.type = (unsigned char) atoi(tempbuf);
    fgets(tempbuf,256,inpack);
    pk.datalen = strlen(tempbuf);
    pk.data = calloc(pk.datalen,1);
    strcpy(pk.data,tempbuf);
    
    // Lazy Man's CRC
    pk.crc = 0xFF;
    int i;
    for (i = 0; i < pk.datalen; i++)
        pk.crc ^= ((char*)pk.data)[i];
    
    
    
    struct addrinfo hints, *res;
    int sockfd;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    
    getaddrinfo(argv[1],argv[2], &hints, &res);
    
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    

    
    //pk.userid = 0xDEAD;
    //pk.type = 0xFD;
    //pk.datalen = 0x9;
    //pk.crc = 0x3D;
    //pk.data = malloc(9);
    //strcpy(pk.data,"DEADBEEF\0");
    
    struct sockaddr dest;
    dest.sa_family = AF_INET;
    strcpy(dest.sa_data,argv[1]);
    
    
    int length;
    void * msg = uctToptr(pk,&length);
    
    // Make a better packet struct -> array function later
    int bytes;
    bytes = sendto(sockfd,msg,length,0,res->ai_addr,sizeof(*(res->ai_addr)));
    printf("Bytes sent: %i\n",bytes);
    if (bytes == -1)
        perror("Send error");
        
    return 0;
}

//int sendto(int sockfd, const void *msg, int len, unsigned int flags,
   //        const struct sockaddr *to, socklen_t tolen); 
