#ifndef _cli_packet_h_
#define _cli_packet_h_

// All the packet types, for the redirector
#define LOGIN 0x1           // Log in request
#define LOGIN_GRANTED 0x2   // Allow the client to start sending messages
#define LOGIN_REJECTED 0x3  // User/Pass mismatch, or revoked user. Reason in [data] section (string)
#define DISCONNECT 0x4      // Disconnect by user
#define ACK 0x5             // Acknowledgement packet
#define MSG 0x6             // Message Packet
#define RESEND 0x7          // Resend last sent packet (failed CRC, etc.)
#define UPDATE_REQ 0x8      // Request for new updates
#define UPDATE_NUM 0x9      // Notification of how many new messages to expect
#define SEND 0xA            // Submission of a new message
#define REVOKE              // User Killswitch, signal to client to remove database, and force disconnect

// #defines from server.h, TODO: Figure out a nicer way to handle this
#define EMPTY_DATA ~0x0
#define USER_SECTION unsigned short
#define TYPE_SECTION unsigned char
#define DATA_SECTION unsigned short
#define CRC_SECTION unsigned char

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct packet
{
    USER_SECTION userid;
    TYPE_SECTION type;
    void* data;
    DATA_SECTION datalen;
    CRC_SECTION crc;
};

void* pk_hton(struct packet pk, int* length);

// Global packet, to set the userid in

#endif
