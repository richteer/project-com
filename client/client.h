// Contains the main while loop for the client.
// Note, most of the below headers are copy-pasted (as usual).
// TODO: Sift through, and remove unneeded headers
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#define EMPTY_DATA ~0x0
#define USER_SECTION unsigned short
#define TYPE_SECTION unsigned char
#define DATA_SECTION unsigned short
#define CRC_SECTION unsigned char

#include "cli-packet.h"

unsigned short quit;
// TODO: Make these in a client_init() function, and non-global
int sockfd; 
struct addrinfo* res;
USER_SECTION userid;

// Core of the client: a while loop continuously taking in user input.
// TODO: Add a fork or something that checks for a received packet
int client_main();

// Handle the user's input. 
//  NOTE: This will be probably useless if the GUI is ever added
int input_handler(char* buffer);

int run_cmd(char* buffer);

int send_login();
