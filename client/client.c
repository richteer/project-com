#include "client.h"

int client_main()
{
    // TODO: Move this to a client_init() function
    struct addrinfo hints;
    userid = 0;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    
    // TODO: Remove the hard coded values and allow the user to set them
    getaddrinfo("127.0.0.1","12345", &hints, &res);
    
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    quit = 0; // Global set in client.h
    
    char* buffer = (char*) malloc(256); // TODO: remove the 256 character limit...later
    int cmderr;
    while (!quit)
    {
        fgets(buffer,256,stdin);
        if ((cmderr = input_handler(buffer)))
        {
            // TODO: Add actual error handling here
            fprintf(stderr,"Error processing command: %i\n",cmderr);
        }
    }
    free(buffer);
    
    freeaddrinfo(res);
    
    
    /* Sending packets:
    int bytes;
    bytes = sendto(sockfd,msg,length,0,res->ai_addr,sizeof(*(res->ai_addr)));
    printf("Bytes sent: %i\n",bytes);
    if (bytes == -1)
        perror("Send error");
    */
    
    

    // If /quit is called
    return 0;
}

int input_handler(char* buffer)
{
    if (buffer[0] == '/')
    {
        if (run_cmd(buffer))
        {
            fprintf(stderr,"Invalid command\n");        
        }
    }
    else
    {
        // It's a message: package and try to send it
    }
    return 0;
}


int run_cmd(char* buffer)
{
    char c;
    unsigned i = 1; // Because we know the command starts with /
    int con = 0;
    while (!con)
    {
        c = buffer[i];
        i++;
        if (c == ' ')
            con = 1;
        else if (c == '\0')
            con = 2;
        else if (c == '\n')
            con = 3;
    }
    char* cmd = (char*) malloc(sizeof(char)*i-1);
    memcpy(cmd,buffer+1,i-2);
    cmd[i-1] = '\0';
    int cmdlen = i; // offset to the first argument of the command

    // TODO: Split some of the longer command handles into separate functions
    if (!strcmp(cmd,"quit"))
    {
        quit = 1;
        free(cmd);
        return 0;
    }
    else if (!strcmp(cmd,"login"))
    {
        free(cmd);
        if (userid != 0)
        {
            fprintf(stderr,"You are already logged in!\n");
            return 0;
        }
        userid = atoi(buffer+cmdlen);
        if (!userid)
        {
            fprintf(stderr,"Error: %s is not a valid userid",buffer+cmdlen);
            return 0;
        }
        if (send_login())
            fprintf(stderr,"Error: Login failed!\n");
        return 0;
    }
    free(cmd);
    return 1;
}

int send_login()
{
    struct packet pk;
    pk.userid = userid;
    pk.type = LOGIN;
    // Temporary, pass is userid unencrypted.
    pk.data = malloc(sizeof(USER_SECTION));
    memcpy(pk.data,&userid,sizeof(USER_SECTION));
    
    pk.datalen = sizeof(USER_SECTION);
    // TODO: Make the CRC more involved than this
    pk.crc = 0xFF;
    int i;
    for (i = 0; i < pk.datalen; i++)
        pk.crc ^= ((char*)pk.data)[i];

    int len;
    // Convert the packet struct to the fancy void array for sending.
    void * buffer = pk_hton(pk,&len);
    
    
    int bytes = sendto(sockfd,buffer,len,0,res->ai_addr,sizeof(*(res->ai_addr)));
    if (bytes == -1)
        perror("Send error");
        
    free(buffer);
    free(pk.data);
    // WAIT FOR RESPONSE HERE -> This is actually important, since the program can't do anything until then
    
    return 0;
}

