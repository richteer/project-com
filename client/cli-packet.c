#include "cli-packet.h"

void* pk_hton(struct packet pk, int* length)
{
    size_t size_user = sizeof(USER_SECTION);
    size_t size_type = sizeof(TYPE_SECTION);
    size_t size_crc = sizeof(CRC_SECTION);
    size_t size_data = pk.datalen;
    *length = size_user+size_type+size_crc+size_data+sizeof(DATA_SECTION);
    void* message = malloc(*length);
    memcpy(&(message[0]),&pk.datalen,sizeof(DATA_SECTION)); // Put the data section length in first
    memcpy(&(message[sizeof(DATA_SECTION)]),&pk.userid,size_user); // Then the senderid
    memcpy(&(message[sizeof(DATA_SECTION)+size_user]),&pk.type,size_type);  // Then the packet type
    // Skip the message itself for now
    
    memcpy(&(message[sizeof(DATA_SECTION)+size_user+size_type]),pk.data,pk.datalen);
    
    memcpy(&(message[size_user+size_type+size_data+sizeof(DATA_SECTION)]),&pk.crc,size_crc); //CRC is last
    
    return message;
}
